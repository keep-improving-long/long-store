import Vue from "vue"
import VueRouter from "vue-router"

// import Home from '@/pages/Home/Home'
// import Order from '@/pages/Order/Order'
// import Search from '@/pages/Search/Search'
// import Me from '@/pages/Me/Me'
// import Login from '@/pages/Login/Login'

//路由组件懒加载
const Home = () => import('@/pages/Home/Home')
const Order = () => import('@/pages/Order/Order')
const Search = () => import('@/pages/Search/Search')
const Me = () => import('@/pages/Me/Me')
const Login = () => import('@/pages/Login/Login')

import Shop from '@/pages/Shop/Shop'
import ShopGoods from '@/pages/Shop/ShopGoods/ShopGoods'
import ShopRatings from '@/pages/Shop/ShopRatings/ShopRatings'
import ShopInfo from '@/pages/Shop/ShopInfo/ShopInfo'
import ConfirmOrder from '@/pages/ConfirmOrder/ConfirmOrder'

Vue.use(VueRouter)

export default new VueRouter({
    routes: [
        {
            path: '/home',
            component: Home,
            meta: {
                showFooter: true
            }
        },
        {
            path: '/order',
            component: Order,
            meta: {
                showFooter: true
            }
        },
        {
            path: '/search',
            component: Search,
            meta: {
                showFooter: true
            }
        },
        {
            path: '/me',
            component: Me,
            meta: {
                showFooter: true
            }
        },
        {
            path: '/login',
            component: Login
        },
        {
            path: '/',
            component: Home,
            meta: {
                showFooter: true
            }
        },
        {
            path: '/shop',
            component: Shop,
            children: [
                {
                    path: '/shop/goods',
                    component: ShopGoods
                },
                {
                    path: '/shop/ratings',
                    component: ShopRatings
                },
                {
                    path: '/shop/info',
                    component: ShopInfo
                },
                {
                    path: '',
                    redirect: '/shop/goods'
                },
            ]
        },
        {
            path: '/confirmorder',
            component: ConfirmOrder
        }
    ]
})