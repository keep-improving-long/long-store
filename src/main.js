import Vue from 'vue'
import {Lazyload} from 'vant'

import App from './App.vue'
import router from '@/router'
import store from './store'

import '@/common/css/reset.css'
import '@/mock/mockServer'
import loading from '@/common/imgs/loading.gif'
import './filters'

Vue.use(Lazyload, {
  loading
})

Vue.config.productionTip = false

new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
}).$mount('#app')
