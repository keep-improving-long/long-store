import Vue from 'vue'
import format from 'date-fns/format'

// 自定义时间格式化过滤器
Vue.filter('date-format', function (value, formatStr='yyyy-mm-dd hh:mm:ss') {
  return format(value, formatStr)
})